<?php
session_start();
include_once "./includes/class/Manager.class.php";
include_once "./includes/connect.php";
$manager = new Manager($db);
if(isset($_POST["submit"])){
    $user = $manager->connectUser($_POST['username'],$_POST['password']);
    if(!empty($user)){
        $_SESSION["user"] = $user[0];
        header("Location:./index.php");

    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <title>Accueil</title>
</head>

<body>
    <header id='connectHeader'>
        <h1>Se connecter</h1>
    </header>
    <div class='connectForm'>
        <div class="headerConnectForm">
            <h3>Connexion</h3>
            <hr>
        </div>
        <div class="connectInputs">
            <form method="POST">
                <div class="inputsTextAndPass">
                    <div class="typeText">
                        <img src="./includes/img/icons/identifiant.png">
                        <input type="text" name="username" placeholder="Identifiant">
                    </div>
                    <div class="typePassword">
                        <img src="./includes/img/icons/cadena.png">
                        <input type="password" name="password" placeholder="Mot de passe">
                    </div>

                </div>
                <button type="submit" name="submit">Connexion</button>
            </form>

        </div>
    </div>
</body>

</html>