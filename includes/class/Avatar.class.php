<?php

class Avatar{
    private string $name;
    private string $description;
    private string $image;

    public function __construct($name,$description,$image){
        $this->setName($name);
        $this->setDescription($description);
        $this->setImage($image);
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name=$name;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setDescription($description){
        $this->description=$description;
    }

    public function getImage(){
        return $this->image;
    }
    
    public function setImage($image){
        $this->image=$image;
    }
}
?>