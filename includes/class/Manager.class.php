<?php
class Manager{
    //attributes

    private $db;

    // constructor 

    public function __construct(PDO $bdd)
    {
        $this->setDb($bdd);
    }

    // getters

    public function getDb()
    {
        return $this->db;
    }

    // setters

    public function setdb(PDO $db)
    {
        $this->db = $db;
    }

    public function addUser(User $user){
        $sql = $this->db->prepare('INSERT INTO `utilisateurs`(`username`,`mail`,`password`)
        VALUES (:username,:mail,:password)
        ');
        $sql->execute([
            'username'=>$user->getUsername(),
            'mail'=>$user->getMail(),
            'password'=>$user->getPassword()

        ]);

    }
    public function connectUser($username,$password){
        $sql = $this->db->prepare("SELECT * FROM `utilisateurs` WHERE `username` = '$username' AND `password` = '$password'
        ");
        $sql->execute();
        return $sql->fetchAll();

    }
    public function addAvatar(Avatar $avatar){
        $sql= $this->db->prepare('INSERT INTO `avatars`(`nom`, `description`, `image`)
        VALUES (:nom,:description,:image)
        ');
        $sql->execute([
            'nom'=>$avatar->getName(),
            'description'=>$avatar->getDescription(),
            'image'=>$avatar->getImage()
        ]);
    }
    public function getAvatar(){
        $sql = $this->db->prepare("SELECT * FROM `avatars`");
        $sql->execute();
        return $sql->fetchAll();
        
    }

}
