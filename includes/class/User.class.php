<?php

class User
{
     private string $username;
     private string $mail;
     private $password;

     public function __construct($username,$mail,$password)
     {
          $this->setUsername($username);
          $this->setMail($mail);
          $this->setPassword($password);
     }
     
     public function getUsername (){
          return $this->username;

     }

     public function setUsername($username){
          $this->username=$username;
          
     }

     public function getMail(){
          return $this->mail;
     }

     public function setMail($mail){
          $this->mail=$mail;
     }
     
     public function getPassword(){
          return $this->password;
     }

     public function setPassword($password){
          $this->password=$password;
     }




}



?>