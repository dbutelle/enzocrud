<?php
    $host = "localhost:3306";
    $dbname = "enzocrud";
    $login = "root";
    $mdp = "root";

    try {
        $db = new PDO('mysql:host='. $host . ';dbname=' . $dbname . ';charset=UTF8', $login, $mdp);

    } catch(Exception $error) {
        die ("Error : ". $error->getMessage());
    }

?>
