<?php
session_start();
include_once("./includes/class/User.class.php");
include_once("./includes/class/Manager.class.php");
include_once("./includes/connect.php");
include_once("./includes/class/Avatar.class.php");
$manager = new Manager($db);
if (isset($_POST["submit"])) {
    move_uploaded_file($_FILES["image"]["tmp_name"], "./includes/img/avatars/" . $_FILES["image"]["name"]);
    $avatar = new Avatar($_POST["name"], $_POST["description"], "./includes/img/avatars/" . $_FILES["image"]["name"]);
    $manager->addAvatar($avatar);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="./profil.js" defer></script>
    <script src="./script.js" defer></script>
    <title>Accueil</title>
</head>

<body>
    <header>
        <div class="mx-5"></div>
        <h1 class="ms-5">Accueil</h1>
        <?php
        if (empty($_SESSION["user"])) {
        ?>
            <button onclick='window.location.href="./connexion.php"'>Se connecter</button>
        <?php


        } else {
        ?>
            <div>
                <div class="dropdown me-5">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <?php echo $_SESSION["user"]["username"]; ?>
                    </a>

                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <li id="profil"><a class="dropdown-item" href="#">Profil</a></li>
                        <li><a class="dropdown-item" href="./logout.php">Déconnexion</a></li>
                    </ul>
                </div>
            </div>

        <?php
        }
        ?>


    </header>
    <?php
    if (empty($_SESSION["user"])) {
    ?>
        <div class="connectMsg">
            <p>Veuillez-vous <a href="./connexion.php">connecter</a> pour accéder à vos avatars</p><br>
        </div>
    <?php
    } else {
    ?>
        <div class="containerAccueil">
            <?php
            $avatars = $manager->getAvatar();
            foreach ($avatars as $avatar) {
            ?>
                <div class="avatar">
                    <?php
                    echo $avatar["nom"];
                    ?>
                </div>
            <?php

            };
            ?>
            <img src="./includes/img/icons/ajouter.png" width="53px">
            <div class="formAddAvatar ninja">
                <h3>Créer votre avatar</h3>
                <form method="POST" enctype='multipart/form-data'>
                    <input type="text" placeholder="Nom de votre avatar" name="name">
                    <textarea name="description" cols="30" rows="10" placeholder="Description"></textarea>
                    <input type="file" name="image">
                    <input type="submit" value="Créer" name="submit">
                </form>


            </div>

        </div>

    <?php
    }
    ?>


    <div class="position-fixed bottom-0 start-0 p-3" style="z-index: 11">
        <div id="liveToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <strong class="me-auto">Alerte</strong>
                <small>Page non trouvée</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                Cette page n'est pas finie.
            </div>
        </div>
    </div>
</body>

</html>